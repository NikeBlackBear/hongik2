package com.example.administrator.homework.model;

import java.util.List;

/**
 * Created by Administrator on 2015-01-27.
 */
public class PopUp {
    private String senderName;
    private String messageText;
    private String chatRoomId;
    private String senderURL;
    private List<String> memberList;

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setSenderURL(String senderURL) {
        this.senderURL = senderURL;
    }

    public String getSenderURL() {
        return senderURL;
    }

    public void setMemberList(List<String> memberList) {
        this.memberList = memberList;
    }

    public List<String> getMemberList() {
        return memberList;
    }

}