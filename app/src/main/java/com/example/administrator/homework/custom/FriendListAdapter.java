package com.example.administrator.homework.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * 친구 목록 리스트 구성을 위한 Adapter
 */
public class FriendListAdapter extends BaseAdapter implements CommonValues {

    private Activity mActivity;
    private LayoutInflater mInflater;
    private JsonArray mFriendListJArry;

    public FriendListAdapter(Activity activity, JsonArray array) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mFriendListJArry = array;
    }

    @Override
    public int getCount() {
        return mFriendListJArry.size();
    }

    @Override
    public JsonObject getItem(int i) {
        return mFriendListJArry.get(i).getAsJsonObject();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.friend_list_row, null);
            holder = new ViewHolder();
            holder.mProfileImage = (ProfileImageView) view.findViewById(R.id.profile);
            holder.mName = (TextView) view.findViewById(R.id.name);
            holder.mDialogText = (TextView) view.findViewById(R.id.dialog_text);
            holder.mDialogArrowImage = (FrameLayout) view.findViewById(R.id.arrow);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.mName.setText(getItem(i).get(PARAM_NAME).getAsString());
        if (getItem(i).get(PARAM_DIAL_TEXT).getAsString().equals("")) {
            holder.mDialogText.setVisibility(View.GONE);
            holder.mDialogArrowImage.setVisibility(View.GONE);
        } else {
            holder.mDialogText.setVisibility(View.VISIBLE);
            holder.mDialogArrowImage.setVisibility(View.VISIBLE);
            holder.mDialogText.setText(getItem(i).get(PARAM_DIAL_TEXT).getAsString());
        }
        holder.mProfileImage.loadImage(getItem(i).get(PARAM_PROFILE_URL).getAsString());
        return view;
    }

    class ViewHolder {
        public ProfileImageView mProfileImage;
        public TextView mName;
        public FrameLayout mDialogArrowImage;
        public TextView mDialogText;
    }
}
