package com.example.administrator.homework.model;

import java.util.List;

/**
 * Created by Administrator on 2015-01-23.
 */
public class ReceiveChatMessage {
    private String chatNo;
    private String sender;
    private String senderName;
    private String messageText;
    private String sendTime;
    private List<String> memberList;
    private String chatRoomId;
    private String roomType; // 초기 생성 - ROOM_INIT , 기존 채팅방 - ROOM_EXIST

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setChatNo(String chatNo) {
        this.chatNo = chatNo;
    }

    public String getChatNo() {
        return chatNo;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setMemberList(List<String> memberList) {
        this.memberList = memberList;
    }

    public List<String> getMemberList() {
        return memberList;
    }

    public void setRoomExist(String roomType) {
        this.roomType = roomType;
    }

    public String getRoomExist() {
        return roomType;
    }


    @Override
    public String toString() {
        return "[chatNo-" + chatNo + "] [sender-" + sender + "] [messageText-" + messageText + "] [sendTime-" + sendTime + "] [memberList-" + memberList + "]";
    }

}
