package com.example.administrator.homework.gcm;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.main.communication;
import com.example.administrator.homework.main.newMessagePopUpActivity;
import com.example.administrator.homework.model.Friend;
import com.example.administrator.homework.model.ReceiveChatMessage;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015-01-07.
 */
public class GcmHService extends IntentService implements CommonValues {

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    public GcmHService() {
        super("GcmHService");
    }

    public GcmHService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);
        LogUtil.LOGV("GcmHService :: onHandleIntent()");
        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            LogUtil.LOGV("GcmHService :: onHandleIntent() :: extra exist messageType = " + messageType);
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                LogUtil.LOGE("GcmHService :: Received :: MESSAGE_TYPE_SEND_ERROR");
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                LogUtil.LOGE("GcmHService :: Received :: Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                LogUtil.LOGD("GcmHService :: Received: " + extras.toString());
                if (extras.getString(GCM_EXTRA_MESSAGE).equals(MSG_TYPE_NEW_USER)) {
                    LogUtil.LOGE("GcmHService :: Received :: MSG_TYPE_NEW_USER");
                    addNewFriendData(extras);
                } else if (extras.getString(GCM_EXTRA_MESSAGE).equals(MSG_TYPE_CHAT)) {
                    LogUtil.LOGE("GcmHService :: Received :: MSG_TYPE_CHAT :: message = " + Utils.getDecodeKorText(extras.getString(PARAM_MESSAGE_PACK)));
                    String messagePack = Utils.getDecodeKorText(extras.getString(PARAM_MESSAGE_PACK));
                    DataManager manager = new DataManager(this);
                    final ReceiveChatMessage receiverCMessage = new Gson().fromJson(messagePack, ReceiveChatMessage.class);
                    manager.insertChatMessageList(receiverCMessage);

                    if (receiverCMessage.getRoomExist().equals(ROOM_TYPE_INIT)) {
                        manager.insertChatRoomList(receiverCMessage);
                    }
                    String senderUrl = manager.getFriendProfileURL(receiverCMessage.getSender());
                    if (!senderUrl.equals("")) {
                        ImageRequest imgRequest = new ImageRequest(senderUrl, new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                LogUtil.LOGE("GcmHService :: imageRequest :: onResponse  :: bitmap = " + response);
                                sendMessageNotification(response, receiverCMessage);
                            }
                        }, 0, 0, Bitmap.Config.ARGB_8888, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                LogUtil.LOGE("GcmHService :: imageRequest :: onErrorResponse");
                            }
                        });
                        ServerProxy.getInstance(this).getRequestQueue().add(imgRequest);
                    } else {
                        sendMessageNotification(null, receiverCMessage);
                    }
                    sendBroadcast(new Intent(ACTION_NEW_CHAT_MESSAGE));
                    LogUtil.LOGE("GcmHService :: 앱실행판단전");
                    if (isShowNewMessagePopup()) {
                        LogUtil.LOGE("GcmHService :: 앱실행판단중(앱실행 X)");
                        Intent popupIntent = new Intent(this, newMessagePopUpActivity.class);
                        PendingIntent pie = PendingIntent.getActivity(getApplicationContext(), 0, popupIntent, PendingIntent.FLAG_ONE_SHOT);
                        try {
                            pie.send();
                        } catch (PendingIntent.CanceledException e) {
                            LogUtil.LOGE(e.getMessage());
                        }
                    }
                }
            }
        } else {
            LogUtil.LOGV("GcmHService :: onHandleIntent() :: extra no exist");
        }
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void addNewFriendData(Bundle friendData) {
        LogUtil.LOGE("addNewFriendData() :: Add New Friend Data !!");
        DataManager manager = new DataManager(this);
        Friend friend = new Friend();
        friend.setAuthId(friendData.getString(PARAM_AUTH_ID));
        friend.setName(Utils.getDecodeKorText(friendData.getString(PARAM_NAME)));
        friend.setPhoneNumber(friendData.getString(PARAM_PHONE_NUMBER));
        friend.setDialText(friendData.getString(PARAM_DIAL_TEXT));
        friend.setBlobKey(friendData.getString(PARAM_BLOB_KEY));
        friend.setProfileUrl(friendData.getString(PARAM_PROFILE_URL));
        manager.insertNewFriendData(friend);
    }

    private void sendMessageNotification(Bitmap iconBitmap, ReceiveChatMessage pack) {
        LogUtil.LOGE("GcmHService :: sendMessageNotification :: iconBitmap = " + iconBitmap);
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, communication.class);
        intent.putStringArrayListExtra(EXTRA_RECEIVER_ID, (ArrayList<String>) pack.getMemberList());

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
        if (iconBitmap != null) {
            mBuilder.setLargeIcon(iconBitmap);
        }
        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle(Utils.getDecodeKorText(pack.getSenderName()));
        mBuilder.setStyle(new NotificationCompat.BigTextStyle()
                .bigText("TEST1"));
        mBuilder.setContentText(Utils.getDecodeKorText(pack.getMessageText()));

        mBuilder.setContentIntent(contentIntent);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);
        mBuilder.setSound(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.message_alarm));
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private boolean isShowNewMessagePopup() {
        LogUtil.LOGE("GcmHService :: isRunningProcess");
        ActivityManager actMng = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = actMng.getRunningTasks(1);
        LogUtil.LOGE("running package = " + taskInfo.get(0).baseActivity.getPackageName());

        if (taskInfo.get(0).baseActivity.getPackageName().equals("com.example.administrator.homework")) {
            return false;
        }

        return true;
//
//        List<ActivityManager.RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
//        if (list.get(0).processName.equals(packageName)) {
//            LogUtil.LOGE("CHECK");
//        } else {
//            LogUtil.LOGE("UNCHECK");
//        }
//        for (ActivityManager.RunningAppProcessInfo rap : list) {
//            LogUtil.LOGE("GcmHService :: isRunningProcess :: processName = " + rap.processName);
//            if (rap.processName.equals(packageName)) {
//                LogUtil.LOGE("GcmHService :: isRunningProcess :: CORRECT");
//                isRunning = true;
//                break;
//            }
//        }
    }

}
