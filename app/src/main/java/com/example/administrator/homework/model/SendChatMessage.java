package com.example.administrator.homework.model;

import java.util.List;


/**
 * Created by Administrator on 2015-01-23.
 */
public class SendChatMessage {
    private String chatRoomId;
    private String sender;
    private String senderName;
    private String messageText;
    private String sendTime;
    private List<String> memberList;

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setMemberList(List<String> memberList) {
        this.memberList = memberList;
    }

    public List<String> getMemberList() {
        return memberList;
    }


    @Override
    public String toString() {
        return "[sender-" + sender + "] [messageText-" + messageText + "] [sendTime-" + sendTime + "] [memberList-" + memberList + "]";
    }

}
