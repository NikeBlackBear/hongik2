package com.example.administrator.homework.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.example.administrator.homework.R;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.service.HService;

/**
 * IntroActivity
 */
public class IntroActivity extends Activity {
    private final int START_MAIN_ACTIVITY_TIME = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        mStartHandler.sendEmptyMessageDelayed(0, START_MAIN_ACTIVITY_TIME);
        Intent intent = new Intent(this, HService.class);
        startService(intent);
    }

    private final Handler mStartHandler = new Handler(
    ) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            DataManager dataManager = new DataManager(IntroActivity.this);
            Intent intent;
            if (dataManager.isExistUserInfo()) {
                intent = new Intent(IntroActivity.this, ParentFragmentActivity.class);
            } else {
                intent = new Intent(IntroActivity.this, AuthActivity.class);
            }
            startActivity(intent);
            finish();
        }
    };
}
