package com.example.administrator.homework.gcm;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.example.administrator.homework.util.LogUtil;

/**
 * Created by Administrator on 2015-01-07.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        LogUtil.LOGE("GcmBroadcastReceiver :: onReceive()");
        ComponentName comp = new ComponentName(context.getPackageName(),
                GcmHService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
