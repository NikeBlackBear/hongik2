package com.example.administrator.homework.main;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.example.administrator.homework.R;
import com.example.administrator.homework.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * 메인 Fragment Activity
 */
public class ParentFragmentActivity extends FragmentActivity {

    private final int TAB_FRIEND_LIST = 100;
    private final int TAB_CHAT = 101;
    private final int TAB_SEARCH = 102;
    private final int TAB_SETTING = 103;

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private FragmentTabHost mTabHost;
    private List<Contact> mContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.parent_fragment_activity);
        init();
    }

    private void init() {
        mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.content);

        mTabHost.addTab(mTabHost.newTabSpec("friend").setIndicator(getTabItemView(TAB_FRIEND_LIST)), FriendFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("chat").setIndicator(getTabItemView(TAB_CHAT)), ChatFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("search").setIndicator(getTabItemView(TAB_SEARCH)), SearchFragment.class, null);
        mTabHost.addTab(mTabHost.newTabSpec("setting").setIndicator(getTabItemView(TAB_SETTING)), SettingFragment.class, null);
        prepareContactList();
    }

    private View getTabItemView(int type) {
        View tabView = new View(this);
        LayoutInflater inflater = getLayoutInflater();
        if (type == TAB_FRIEND_LIST) {
            tabView = inflater.inflate(R.layout.tab_friend_list, null);
        } else if (type == TAB_CHAT) {
            tabView = inflater.inflate(R.layout.tab_chat, null);
        } else if (type == TAB_SEARCH) {
            tabView = inflater.inflate(R.layout.tab_search, null);
        } else if (type == TAB_SETTING) {
            tabView = inflater.inflate(R.layout.tab_setting, null);
        }
        return tabView;
    }

    private void prepareContactList() {
        Uri people = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        mContacts = new ArrayList<Contact>();
        Cursor cursor = managedQuery(people, projection, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                Contact acontact = new Contact();
                acontact.setId(cursor.getString(0));
                acontact.setName(cursor.getString(1));
                acontact.setPhone(cursor.getString(2));
                mContacts.add(acontact);
            } while (cursor.moveToNext());
        }
    }

    public List<Contact> getContacts() {
        return mContacts;
    }
}