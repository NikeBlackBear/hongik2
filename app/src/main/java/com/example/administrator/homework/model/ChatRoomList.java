package com.example.administrator.homework.model;

import java.util.List;

/**
 * Created by Administrator on 2015-01-23.
 */
//대화방 생성할때 필요한 목록
public class ChatRoomList {
    private String messageText;
    private String sendTime;
    private List<String> memberList;
    private String chatRoomId;
    private String senderURL;
    private String memberListName;

    public void setChatRoomId(String chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getChatRoomId() {
        return chatRoomId;
    }

    public void setSenderURL(String senderURL) {
        this.senderURL = senderURL;
    }

    public String getSenderURL() {
        return senderURL;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setMemberList(List<String> memberList) {
        this.memberList = memberList;
    }

    public List<String> getMemberList() {
        return memberList;
    }

    public void setMemberListName(String memberListName) {
        this.memberListName = memberListName;
    }

    public String getMemberListName() {
        return memberListName;
    }

    @Override
    public String toString() {
        return "[messageText-" + messageText + "] [sendTime-" + sendTime + "] [memberList-" + memberList + "]";
    }
}
