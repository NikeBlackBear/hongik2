package com.example.administrator.homework.custom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.main.communication;
import com.example.administrator.homework.util.LogUtil;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Administrator on 2014-12-26.
 */
public class UserInfoDialog extends AlertDialog implements CommonValues {

    private Context mContext;
    private ProfileImageView mProfileImage;
    private TextView mNameView;
    private Button mPhoneNumberView;
    private TextView mDialogTextView;
    private Button mChatBtn;
    private JsonObject mTargetUserInfo;
    private DataManager mDataManager;

    public UserInfoDialog(JsonObject userInfo, Context context) {
        super(context);
        mContext = context;
        mTargetUserInfo = userInfo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_info_dialog_view);
        mDataManager = new DataManager(mContext);
        mProfileImage = (ProfileImageView) findViewById(R.id.profile_img);
        mNameView = (TextView) findViewById(R.id.user_info_name);
        mPhoneNumberView = (Button) findViewById(R.id.user_info_phone_number);
        mDialogTextView = (TextView) findViewById(R.id.user_info_dial_text);
        mChatBtn = (Button) findViewById(R.id.chat_btn);
        mPhoneNumberView.setOnClickListener(mPhoneBtnClickListener);
        mChatBtn.setOnClickListener(mChatBtnClickListener);
        build();
    }

    private void build() {
        mProfileImage.loadImage(mTargetUserInfo.get(PARAM_PROFILE_URL).getAsString());
        mNameView.setText(mTargetUserInfo.get(PARAM_NAME).getAsString());
        mPhoneNumberView.setText(PhoneNumberUtils.formatNumber(mTargetUserInfo.get(PARAM_PHONE_NUMBER).getAsString()));
        mDialogTextView.setText(mTargetUserInfo.get(PARAM_DIAL_TEXT).getAsString());
    }

    private final View.OnClickListener mPhoneBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + PhoneNumberUtils.formatNumber(mTargetUserInfo.get(PARAM_PHONE_NUMBER).getAsString())));
            mContext.startActivity(intent);
            dismiss();
        }
    };

    private final View.OnClickListener mChatBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(mContext, communication.class);
            ArrayList<String> memberList = new ArrayList<String>();
            memberList.add(mTargetUserInfo.get(PARAM_AUTH_ID).getAsString());
            memberList.add(mDataManager.selectAuthIdUserInfo());
            Collections.sort(memberList);
            LogUtil.LOGD("UserInfoDialog :: mChatBtnClickListener :: memberList = " + memberList);
            intent.putStringArrayListExtra(EXTRA_RECEIVER_ID, memberList);
            mContext.startActivity(intent);
            dismiss();
        }
    };
}
