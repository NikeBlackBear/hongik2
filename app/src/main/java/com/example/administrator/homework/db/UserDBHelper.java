package com.example.administrator.homework.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// Opener of DB and Table
public class UserDBHelper extends SQLiteOpenHelper implements DBValues {

    public UserDBHelper(Context context) {
        super(context, "user_database", null, 1);
    }

    // 생성된 DB가 없을 경우에 한번만 호출됨
    @Override
    public void onCreate(SQLiteDatabase arg0) {
        String createSql = "create table " + TABLE_USER_INFO + " ("
                + COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_AUTH_ID + " text, "
                + COLUMN_NAME + " text, "
                + COLUMN_DIAL_TEXT + " text, "
                + COLUMN_BLOB_KEY + " text, "
                + COLUMN_PROFILE_URL + " text)";
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }
}