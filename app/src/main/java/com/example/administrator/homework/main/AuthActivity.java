package com.example.administrator.homework.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.LoadingView;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.Contact;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 사용자 정보를 입력하여 인증 절차를 진행하는 Activity
 * 서버에 사용자정보와 전화번호를 입력하면 고유의 Key 값을 전달받은 뒤 내부 DB 에 저장 한다.
 */
public class AuthActivity extends Activity implements CommonValues {
    private RelativeLayout mContent;
    private EditText mInputUserName;
    private Button mSubmitBtn;
    private LoadingView mLoadingView;

    private RotateAnimation mLoadingAnimation;

    private ServerProxy mServerProxy;
    private DataManager mDataManager;

    private GoogleCloudMessaging mGcm;

    private List<Contact> mContacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_auth);
        init();
    }

    private void init() {
        mServerProxy = ServerProxy.getInstance(this);
        mDataManager = new DataManager(this);

        mContent = (RelativeLayout) findViewById(R.id.content);
        mInputUserName = (EditText) findViewById(R.id.input_name);
        mSubmitBtn = (Button) findViewById(R.id.submit_btn);
        mInputUserName.addTextChangedListener(mTextWatcher);
        mSubmitBtn.setOnClickListener(mSubmitBtnListener);

        mLoadingView = (LoadingView) findViewById(R.id.loading_area);
        mLoadingAnimation = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        mLoadingAnimation.setInterpolator(new LinearInterpolator());
        mLoadingAnimation.setRepeatCount(Animation.INFINITE);
        mLoadingAnimation.setDuration(1000);

        if (checkPlayServices()) {
            mGcm = GoogleCloudMessaging.getInstance(this);
            String regid = getRegistrationId();

            LogUtil.LOGV("AuthActivity :: onCreate() regId = " + regid);
            if (regid.isEmpty()) {
                LogUtil.LOGV("AuthActivity :: onCreate() regId is empty");
                registerInBackground();
            }
        } else {
            LogUtil.LOGD("No valid Google Play Services APK found.");
        }
        prepareContactList();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                LogUtil.LOGE("This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Gets the current registration ID for application on GCM service.
     * <p/>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     * registration ID.
     */
    private String getRegistrationId() {
        final SharedPreferences prefs = getGCMPreferences();
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            LogUtil.LOGD("Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(this);
        if (registeredVersion != currentVersion) {
            LogUtil.LOGD("App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGCMPreferences() {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(ParentFragmentActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p/>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] objects) {
                String msg = "";
                LogUtil.LOGE("registerInBackground()");
                try {
                    if (mGcm == null) {
                        mGcm = GoogleCloudMessaging.getInstance(AuthActivity.this);
                    }
                    String regid = mGcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    LogUtil.LOGE("registerInBackground() msg = " + msg);
                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(AuthActivity.this, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    ex.printStackTrace();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                LogUtil.LOGE("registerInBackground() :: onPostExecute()");
            }
        }.execute(null, null, null);
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        // Your implementation here.
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences();
        int appVersion = getAppVersion(context);
        LogUtil.LOGD("Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

//    private void testGCM(){
//        new AsyncTask() {
//            @Override
//            protected Object doInBackground(Object[] objects) {
//                LogUtil.LOGV("testGCM()");
//                String msg = "";
//                try {
//                    Bundle data = new Bundle();
//                    data.putString("my_message", "Hello World");
//                    data.putString("my_action",
//                            "com.google.android.gcm.demo.app.ECHO_NOW");
//                    String id = Integer.toString(msgId.incrementAndGet());
//                    LogUtil.LOGV("testGCM() :: id = " + id);
//                    gcm.send(SENDER_ID + "@gcm.googleapis.com", id, data);
//                    msg = "Sent message";
//                } catch (IOException ex) {
//                    msg = "Error :" + ex.getMessage();
//                    ex.printStackTrace();
//                }
//
//                LogUtil.LOGV("testGCM() msg = " + msg);
//                return msg;
//            }
//
//            @Override
//            protected void onPostExecute(Object o) {
//                super.onPostExecute(o);
//            }
//        }.execute(null, null, null);
//    }

    private void prepareContactList() {
        Uri people = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.Phone._ID,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER
        };

        mContacts = new ArrayList<Contact>();
        Cursor cursor = managedQuery(people, projection, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                Contact acontact = new Contact();
                acontact.setId(cursor.getString(0));
                acontact.setName(cursor.getString(1));
                acontact.setPhone(cursor.getString(2));
                mContacts.add(acontact);
            } while (cursor.moveToNext());
        }
    }

    private void showAnimationWithDismissContent() {
        // mContent.setVisibility(View.GONE);
        mLoadingView.setVisibility(View.VISIBLE);
        mLoadingView.startLoadingAnimation(mLoadingAnimation);
    }

    private String getInputUserName() {
        return mInputUserName.getText().toString();
    }

    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (mInputUserName.getText().length() > 0) {
                mSubmitBtn.setEnabled(true);
            } else {
                mSubmitBtn.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private final View.OnClickListener mSubmitBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showAnimationWithDismissContent();
            mServerProxy.requestUserAuth(mAuthResponseCallback, getInputUserName(), Utils.getPhoneNumber(AuthActivity.this), getRegistrationId());
        }
    };

    private final ServerProxy.ResponseCallback mNotifyResponseCallback = new ServerProxy.ResponseCallback() {

        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("AuthActivity :: mNotifyResponseCallback :: onResponse() :: response = " + response);
            Intent intent = new Intent(AuthActivity.this, ParentFragmentActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void onError(int status) {

        }
    };

    private final ServerProxy.ResponseCallback mAuthResponseCallback = new ServerProxy.ResponseCallback() {

        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("AuthActivity :: mAuthResponseCallback :: onResponse() :: response = " + response);
            if (!response.equals("")) {
                mDataManager.insertUserInfo(response, getInputUserName());
                mServerProxy.requestNotifyAboutNewUser(mNotifyResponseCallback, mContacts, response);
            } else {
                Toast.makeText(AuthActivity.this, "인증실패", Toast.LENGTH_SHORT).show();
                LogUtil.LOGE("AuthActivity :: mAuthResponseCallback :: onResponse() :: 인증실패");
            }
        }

        @Override
        public void onError(int status) {

        }
    };
}
