package com.example.administrator.homework.common;

/**
 * Created by Administrator on 2014-12-29.
 */
public interface CommonValues {

    //파라미터 영역
    public static final String PARAM_ID = "id";
    public static final String PARAM_AUTH_ID = "authId";
    public static final String PARAM_GCM_ID = "gcmId";
    public static final String PARAM_NAME = "name";
    public static final String PARAM_PHONE_NUMBER = "phoneNumber";
    public static final String PARAM_DIAL_TEXT = "dialText";
    public static final String PARAM_CONTACTS = "contacts";
    public static final String PARAM_BLOB_KEY = "blobKey";
    public static final String PARAM_PROFILE_URL = "profileUrl";
    public static final String PARAM_MESSAGE_PACK = "messagePack";
    public static final String PARAM_MESSAGE_TEXT = "messageText";
    public static final String PARAM_MEMBER_NAME = "memberName";
    public static final String PARAM_SENDER_NAME = "senderName";
    public static final String PARAM_SENDER_URL = "senderURL";
    public static final String PARAM_SENDER = "sender";

    public static final String ROOM_TYPE_INIT = "init";
    public static final String ROOM_TYPE_EXISTS = "exists";

    //URL 영역
    public static final String URL_USER_AUTH = "http://2-dot-quixotic-module-766.appspot.com/userauthorization";
    public static final String URL_CHECK_FRIENDLIST = "http://2-dot-quixotic-module-766.appspot.com/checkfriendlist";
    public static final String URL_NOTIFY_ABOUT_NEW_USER = "http://2-dot-quixotic-module-766.appspot.com/notifyaboutnewuser";
    public static final String URL_REQUEST_IMAGE_UPLOAD_URL = "http://2-dot-quixotic-module-766.appspot.com/requestUploadUrl";
    public static final String URL_SENDING_MESSAGE = "http://2-dot-quixotic-module-766.appspot.com/sendchatmessage";

    //Preference
    public static final String PREF_KEY_DATA_CHANGED = "key_data_changed";

    //GCM 관련
    public static final String SENDER_ID = "471871548748"; //Do Not Changed.
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String MSG_TYPE_NEW_USER = "Detecting New User.";
    public static final String MSG_TYPE_CHAT = "chat_message";
    public static final String GCM_EXTRA_MESSAGE = "message";

    public static final String EXTRA_RECEIVER_ID = "receiver_id";

    //Contact Sync Task 시작 되고 첫 Sync 가 시작 될 딜레이 시간
    public static final int INIT_CONTACT_SYNC_PERIOD = 5000;
    //첫 Sync 이후에 Sync 동작이 이루어질 Delay 시간
    public static final int DELAY_CONTACT_SYNC_PERIOD = 6000000;

    public static final int REQUEST_CHAT_CODE = 100;
    public static final int RESULT_FINISH_CHAT = 101;


    public static final String ACTION_NEW_CHAT_MESSAGE = "com.example.administrator.homework.ACTION_NEW_CHAT_MESSAGE";
}
