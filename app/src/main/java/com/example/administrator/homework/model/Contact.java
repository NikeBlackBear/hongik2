package com.example.administrator.homework.model;

public class Contact {
    private String id;
    private String name;
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phoneNumber;
    }

    public void setPhone(String phone) {
        phoneNumber = phone;
    }

    public String toString() {
        return name + phoneNumber;
    }
}
