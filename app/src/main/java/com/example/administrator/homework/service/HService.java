package com.example.administrator.homework.service;

import android.app.Service;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Administrator on 2014-12-31.
 */
public class HService extends Service implements CommonValues {
    private ContactContentObserver mObserver;
    private Timer mSyncTimer;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        LogUtil.LOGE("HService :: onCreate()");
        init();
    }

    private void init() {
        mObserver = new ContactContentObserver();
        getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, mObserver);
        mSyncTimer = new Timer();
        mSyncTimer.schedule(new SyncTask(), INIT_CONTACT_SYNC_PERIOD, DELAY_CONTACT_SYNC_PERIOD); // 5초후 첫실행, 1시간마다 계속실행
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(mObserver);
        if (mSyncTimer != null) {
            mSyncTimer.cancel();
        }
    }

    private class ContactContentObserver extends ContentObserver {

        public ContactContentObserver() {
            super(null);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            LogUtil.LOGV("ContactContentObserver :: onChange()");
            Utils.setExistsChangedData(HService.this, true);
        }

        @Override
        public boolean deliverSelfNotifications() {
            LogUtil.LOGV("ContactContentObserver :: deleverSelfNotifications()");
            return super.deliverSelfNotifications();
        }
    }

    private class SyncTask extends TimerTask {

        @Override
        public void run() {
            LogUtil.LOGE("HService :: SyncTask :: run()");
            Utils.setExistsChangedData(HService.this, true);
        }
    }
}
