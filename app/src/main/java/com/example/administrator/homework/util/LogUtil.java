package com.example.administrator.homework.util;

//DDMS 로 부터 디버깅을 위한 Log util 클래스
public class LogUtil {
    private final static String TAG = "Homework";
    private final static boolean DEBUG = true;

    public static void LOGV(String msg) {
        if (DEBUG) {
            android.util.Log.v(TAG, msg);
        }
    }

    public static void LOGE(String msg) {
        if (DEBUG) {
            android.util.Log.e(TAG, msg);
        }
    }

    public static void LOGD(String msg) {
        if (DEBUG) {
            android.util.Log.d(TAG, msg);
        }
    }
}
