package com.example.administrator.homework.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.ProfileImageView;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.PopUp;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-01-27.
 */
public class newMessagePopUpActivity extends Activity implements CommonValues {
    private ProfileImageView mProfileImg;
    private TextView mSenderNameTv;
    private TextView mMessageText;
    private Button mCloseBtn;
    private Button mSelectMessageBtn;
    private DataManager mDataManager;
    private PopUp mPopUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.alertdialog);
        init();
    }

    private void init() {
        mDataManager = new DataManager(getApplicationContext());
        mProfileImg = (ProfileImageView) findViewById(R.id.profile_img);
        mSenderNameTv = (TextView) findViewById(R.id.sender_name_tv);
        mMessageText = (TextView) findViewById(R.id.message_text);
        mCloseBtn = (Button) findViewById(R.id.close_btn);
        mSelectMessageBtn = (Button) findViewById(R.id.select_message_btn);

        mPopUp = mDataManager.getLastMessageFromDB();

        mProfileImg.loadImage(mPopUp.getSenderURL());
        mSenderNameTv.setText(mPopUp.getSenderName());
        mMessageText.setText(mPopUp.getMessageText());
        mSelectMessageBtn.setOnClickListener(mConfirmBtnClickListener);
        mCloseBtn.setOnClickListener(mClosemBtnClickListener);
    }

    private final View.OnClickListener mConfirmBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(newMessagePopUpActivity.this, communication.class);
            intent.putStringArrayListExtra(EXTRA_RECEIVER_ID, (ArrayList<String>) mPopUp.getMemberList());
            startActivity(intent);
        }
    };

    private final View.OnClickListener mClosemBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };


}
