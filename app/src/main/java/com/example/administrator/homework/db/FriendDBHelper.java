package com.example.administrator.homework.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2014-12-31.
 */
public class FriendDBHelper extends SQLiteOpenHelper implements DBValues {

    public FriendDBHelper(Context context) {
        super(context, "friend_database", null, 1);
    }

    // 생성된 DB가 없을 경우에 한번만 호출됨
    @Override
    public void onCreate(SQLiteDatabase arg0) {
        String createSql = "create table " + TABLE_FRIEND_INFO + " ("
                + COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_AUTH_ID + " text, "
                + COLUMN_NAME + " text, "
                + COLUMN_PHONE_NUMBER + " text, "
                + COLUMN_DIAL_TEXT + " text, "
                + COLUMN_BLOB_KEY + " text, "
                + COLUMN_PROFILE_URL + " text)";
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }
}
