package com.example.administrator.homework.main;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.ChatRoomListAdapter;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.ChatRoomList;
import com.example.administrator.homework.util.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends AbsFragment implements CommonValues {
    private ListView mListView;
    private Button mInviteFriendBtn;
    private ChatRoomListAdapter mChatRoomListAdapter;
    private DataManager mDataManager;
    private List<ChatRoomList> mChatRoomData;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        View root = getView();
        mListView = (ListView) root.findViewById(R.id.talkRoomList);
        mListView.setOnItemClickListener(mChatListItemClickListener);
        mInviteFriendBtn = (Button) root.findViewById(R.id.invite_friend_btn);
        mInviteFriendBtn.setOnClickListener(mInviteFriendBtnListener);

        mDataManager = new DataManager(getActivity());
        mChatRoomData = mDataManager.getChatRoomListToDB();

        mChatRoomListAdapter = new ChatRoomListAdapter(getActivity(), mChatRoomData);
        mListView.setAdapter(mChatRoomListAdapter);

        IntentFilter intentFilter = new IntentFilter(ACTION_NEW_CHAT_MESSAGE);
        getActivity().registerReceiver(receiver, intentFilter);
        LogUtil.LOGD("BroadcastReceiver :: 등록됨");
    }

    private void refreshChatList() {
        mChatRoomData.clear();
        List<ChatRoomList> dummyList = mDataManager.getChatRoomListToDB();
        for (int i = 0; i < dummyList.size(); i++) {
            mChatRoomData.add(dummyList.get(i));
        }
        mChatRoomListAdapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener mChatListItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Intent intent = new Intent(getActivity(), communication.class);
            intent.putStringArrayListExtra(EXTRA_RECEIVER_ID, (ArrayList<String>) mChatRoomData.get(i).getMemberList());
            startActivityForResult(intent, REQUEST_CHAT_CODE);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHAT_CODE) {
            LogUtil.LOGD("ChatFragment :: onActivityResult() resultCode = " + resultCode);
            refreshChatList();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(receiver);
        LogUtil.LOGD("BroadcastReceiver :: 종료됨");
    }

    private final View.OnClickListener mInviteFriendBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), InviteFriendActivity.class);
            startActivityForResult(intent, REQUEST_CHAT_CODE);
        }
    };

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String name = intent.getAction();
            LogUtil.LOGD("BroadcastReceiver :: " + name);
            if (name.equals(ACTION_NEW_CHAT_MESSAGE)) {
                refreshChatList();
            }
        }
    };
}