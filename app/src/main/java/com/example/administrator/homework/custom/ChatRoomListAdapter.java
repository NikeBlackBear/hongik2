package com.example.administrator.homework.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.model.ChatRoomList;
import com.example.administrator.homework.util.Utils;

import java.util.Collections;
import java.util.List;

public class ChatRoomListAdapter extends BaseAdapter implements CommonValues {

    private Activity mActivity;
    private List<ChatRoomList> mChatRoomList;
    private LayoutInflater mInflater;

    public ChatRoomListAdapter(Activity activity, List<ChatRoomList> list) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mChatRoomList = list;
        Collections.reverse(mChatRoomList);
    }

    @Override
    public int getCount() {
        return mChatRoomList.size();
    }

    @Override
    public ChatRoomList getItem(int i) {
        return mChatRoomList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.talkroom_list, null);
            holder = new ViewHolder();
            holder.tv_lastmessage = (TextView) view.findViewById(R.id.Last_message);
            holder.tv_lastmessage_date = (TextView) view.findViewById(R.id.Last_message_date);
            holder.tv_lastmessage_time = (TextView) view.findViewById(R.id.Last_message_time);
            holder.tv_memberlist = (TextView) view.findViewById(R.id.member_list);
            holder.iv_profile = (ProfileImageView) view.findViewById(R.id.talkRoom_profile_image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tv_memberlist.setText(getItem(i).getMemberListName());
        holder.tv_lastmessage.setText(getItem(i).getMessageText());
        holder.tv_lastmessage_date.setText(Utils.timeEncoder(getItem(i).getSendTime())[0]);
        holder.tv_lastmessage_time.setText(Utils.timeEncoder(getItem(i).getSendTime())[1]);
        holder.iv_profile.loadImage(getItem(i).getSenderURL());
        return view;
    }

    class ViewHolder {
        public TextView tv_memberlist;
        public TextView tv_lastmessage;
        public TextView tv_lastmessage_time;
        public TextView tv_lastmessage_date;
        public ProfileImageView iv_profile;
    }
}