package com.example.administrator.homework.db;

/**
 * Created by Administrator on 2014-12-29.
 */
public interface DBValues {
    public static final String TABLE_USER_INFO = "table_user_info";
    public static final String TABLE_FRIEND_INFO = "table_friend_info";
    public static final String TABLE_CHAT_ROOM_LIST = "table_chat_room_list";
    public static final String TABLE_CHAT_MESSAGE = "table_chat_message";

    public static final String COLUMN_CHAT_ROOM_ID = "chat_room_id";
    public static final String COLUMN_SENDER = "sender";
    public static final String COLUMN_MESSAGE_TEXT = "message_text";
    public static final String COLUMN_SEND_TIME = "send_time";
    public static final String COLUMN_MEMBER_LIST = "member_list";
    public static final String COLUMN_SENDER_NAME = "sender_name";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_AUTH_ID = "auth_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PHONE_NUMBER = "phone_number";
    public static final String COLUMN_DIAL_TEXT = "dial_text";
    public static final String COLUMN_BLOB_KEY = "blob_key";
    public static final String COLUMN_PROFILE_URL = "profile_url";

    public static final String NOT_EXISTS_DATA = "not_exists_data";
}
