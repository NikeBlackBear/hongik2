package com.example.administrator.homework.proxy;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.util.LogUtil;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class MultipartRequest extends Request<String> implements CommonValues {

    MultipartEntityBuilder entity = MultipartEntityBuilder.create();
    HttpEntity httpentity;
    private static final String FILE_PART_NAME = "file";

    private final Response.Listener<String> mListener;
    Map<String, String> headers = new HashMap<String, String>();

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public MultipartRequest(String url, File file, String authId, String blobKey, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);

        mListener = listener;

        entity.addPart(FILE_PART_NAME, new FileBody(file));
        entity.addBinaryBody("file", file);
        entity.addTextBody(PARAM_AUTH_ID, authId);
        entity.addTextBody(PARAM_BLOB_KEY, blobKey);
        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        entity.build();
    }

    public MultipartRequest(String url, File file, Response.Listener<String> listener, Response.ErrorListener errorListener,
                            Map<String, String> mStringPart) {
        super(Method.POST, url, errorListener);

        mListener = listener;
        //entity.addPart(FILE_PART_NAME, new FileBody(file));

        if (mStringPart != null && mStringPart.isEmpty()) {
            for (Map.Entry<String, String> entry : mStringPart.entrySet()) {
                entity.addTextBody(entry.getKey(), entry.getValue());
            }
        }

        entity.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
    }

    public void setEntity(MultipartEntityBuilder entity) {
        this.entity = entity;
    }

    @Override
    public String getBodyContentType() {
        return httpentity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            httpentity = entity.build();
            httpentity.writeTo(bos);
            LogUtil.LOGE("getBody() :: httpentity = " + httpentity.toString());
        } catch (IOException e) {
            LogUtil.LOGE("IOException writing to ByteArrayOutputStream");
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String jsonString = "";
        try {
            jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(jsonString, getCacheEntry());
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }

}
