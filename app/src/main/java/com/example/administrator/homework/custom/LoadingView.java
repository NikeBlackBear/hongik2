package com.example.administrator.homework.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.administrator.homework.R;

/**
 * Created by Administrator on 2014-12-29.
 */
public class LoadingView extends RelativeLayout{
    private ImageView mLoadingImg;
    public LoadingView(Context context) {
        this(context, null, 0);
    }
    public LoadingView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public LoadingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLoadingImg = (ImageView) findViewById(R.id.loadingImg);
    }

    public void startLoadingAnimation(RotateAnimation anim){
        mLoadingImg.startAnimation(anim);
    }

    public void stopLoadingAnimation(){
        mLoadingImg.setAnimation(null);
    }
}
