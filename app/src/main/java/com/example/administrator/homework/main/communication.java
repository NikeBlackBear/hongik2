package com.example.administrator.homework.main;

import android.app.ActionBar;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.ProfileImageView;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.ReceiveChatMessage;
import com.example.administrator.homework.model.SendChatMessage;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;


public class communication extends ActionBarActivity implements CommonValues {
    private Button mSubmitbtn;
    private EditText mEdt;
    private LinearLayout mLayout;
    private TextView mText;
    private int mCount;
    private ScrollView mScroll;
    private LayoutInflater mLayinflater;
    private DataManager mDataManager;
    private String mSender, mTime, mMessage, mChatRoomId, mChetNo, mRoomExist, mSenderName; //보낸사람, 날짜-시간, 메세지, 채팅방 ID, 보내는 사람 프로필사진 URL
    private List<String> mMember; // 참여자 정보
    private Utils timedata;
    private String mMesaagerSender, mReceiveMessage, mReceiveTime, mReceiveSendername, mReceiveSenderUrl;
    private List<ReceiveChatMessage> receiveMessages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);
        mScroll = (ScrollView) findViewById(R.id.scroll);
        mDataManager = new DataManager(getApplicationContext());
        timedata = new Utils();
        mSubmitbtn = (Button) findViewById(R.id.submit);
        mEdt = (EditText) findViewById(R.id.editText);
        mLayout = (LinearLayout) findViewById(R.id.relative01);
        mLayinflater = LayoutInflater.from(this);
        ActionBar actionBar = getActionBar();
        actionBar.hide();

        mSender = mDataManager.selectAuthIdUserInfo();//보낸사람
        mSenderName = mDataManager.selectNameUserInfo();//보낸사람이름
        mMember = new ArrayList<String>();//참여자
        if (getIntent() != null && getIntent().getExtras() != null) {
            mMember = getIntent().getStringArrayListExtra(EXTRA_RECEIVER_ID);
        }
        String memberListToString = mDataManager.getAppendedMemberList(mMember);
        mChatRoomId = mDataManager.getChatRoomIdToDB(memberListToString); // 채팅방 아이디

        //메세지 50개 불러오기
        receiveMessages = mDataManager.loadMessage(mChatRoomId);
        for(int i= receiveMessages.size()-1; i>=0  ; i--){
            mMesaagerSender = receiveMessages.get(i).getSender();
            mReceiveMessage = receiveMessages.get(i).getMessageText();
            mReceiveTime = Utils.timeEncoder(receiveMessages.get(i).getSendTime())[1];
            mReceiveSendername = receiveMessages.get(i).getSenderName();
            mReceiveSenderUrl = mDataManager.getFriendProfileURL(mMesaagerSender);
            LogUtil.LOGV("communication :: for "+mMesaagerSender+ "//"+mReceiveMessage+"//"+ mReceiveTime +"//"+mReceiveSendername + "//"+mReceiveSenderUrl );
            addtext(mReceiveMessage, mMesaagerSender);
        }
        scrollToEnd();
        messageListnner();

        // 보내기 버튼 클릭
        mSubmitbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageForm();
                mEdt.setText("");
            }
        });
    }

    public  void messageListnner(){
        IntentFilter intentFilter = new IntentFilter(ACTION_NEW_CHAT_MESSAGE);
        this.registerReceiver(receiver, intentFilter);
        LogUtil.LOGD("BroadcastReceiver :: 등록됨");
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        this.unregisterReceiver(receiver);
        LogUtil.LOGD("BroadcastReceiver :: 종료됨");
    }
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String name = intent.getAction();
            LogUtil.LOGD("BroadcastReceiver :: " + name);
                if(name.equals(ACTION_NEW_CHAT_MESSAGE)) {
                    ReceiveChatMessage aa = new ReceiveChatMessage();
                    aa = mDataManager.loadfinalMessage(mChatRoomId);
                    mMesaagerSender = aa.getSender();
                    mReceiveMessage = aa.getMessageText();
                    mReceiveTime = Utils.timeEncoder(aa.getSendTime())[1];
                    mReceiveSendername = aa.getSenderName();
                    mReceiveSenderUrl = mDataManager.getFriendProfileURL(mMesaagerSender);
                    LogUtil.LOGV("communication :: for " + mMesaagerSender + "//" + mReceiveMessage + "//" + mReceiveTime + "//" + mReceiveSendername + "//" + mReceiveSenderUrl);
                    addtext(mReceiveMessage, mMesaagerSender);
                    removemessage();
                    scrollToEnd();
            }
        }
    };

    public void addtext(String Message, String senderidid) {
        View v = mLayinflater.inflate(R.layout.talklayout, null);
        TextView talkAP = (TextView) v.findViewById(R.id.tv_message);
        ProfileImageView friendphoto = (ProfileImageView)v.findViewById(R.id.friendphoto);
        TextView talkname = (TextView)v.findViewById(R.id.textView_name);
        TextView talktime = (TextView)v.findViewById(R.id.tv_time);

        friendphoto.loadImage(mReceiveSenderUrl);
        talkname.setText(mReceiveSendername);
        talktime.setText(mReceiveTime);

        mText = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LogUtil.LOGD("communication :: addText :: mSender = " + senderidid + mMesaagerSender );
        if(mSender.equals(senderidid)){
            mText.setText(Message);
            params.gravity = Gravity.RIGHT;
            params.setMargins(10,10,10,10);
            mText.setBackgroundResource(R.drawable.talkboxme);
            mText.setPadding(20, 10, 40, 10);
            mText.setMaxWidth(500);
            mText.setTextSize(20);
            mText.setLayoutParams(params);
            mLayout.addView(mText);
        }
        else {
            talkAP.setText(Message);
            params.gravity = Gravity.LEFT;
            talkAP.setBackgroundResource(R.drawable.talkboxyou);
            talkAP.setPadding(40, 10, 20, 10);
            talkAP.setMaxWidth(400);
            talkAP.setTextSize(20);
            v.setLayoutParams(params);
            mLayout.addView(v);
        }
    }

    //메세지 50개이상 출력시 가장위의 메세지 제거
    public void removemessage(){
        if(mLayout.getChildCount()>50){
            mLayout.removeView(mLayout.getChildAt(0));
            LogUtil.LOGE("메세지~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~몇개니??????? :: "+mLayout.getChildCount());
        }
    }

    //뷰의 아래쪽 보기
    public void scrollToEnd(){
        LogUtil.LOGE("페이지 내려간다요~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        mScroll.post(new Runnable() {
            @Override
            public void run() {
                mScroll.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    public void MessageForm() {
        mTime = timedata.getCurrentTime();// 날짜-시간
        mMessage = mEdt.getText().toString();//메세지
        LogUtil.LOGD("communication :: MessageForm() :: member size = " + mMember.size());
        SendChatMessage a = new SendChatMessage();
        a.setMessageText(mMessage);
        a.setSender(mSender);
        a.setSenderName(mSenderName);
        a.setSendTime(mTime);
        a.setChatRoomId(mChatRoomId);
        a.setMemberList(mMember);
        LogUtil.LOGE("보낸사람 = " + mSender + "날짜 - 시간 =" + mTime + "메세지 =" + mMessage + "참여자 =" + mMember + "대화방 ID =" + mChatRoomId);
        Gson gson = new Gson();
        LogUtil.LOGE(gson.toJson(a));
        submitMessage(gson.toJson(a));
    }

    public void submitMessage(String message) {
        ServerProxy send = ServerProxy.getInstance(getApplicationContext());
        send.requestMessage(new ServerProxy.ResponseCallback() {
            @Override
            public void onResponse(String response) {
                LogUtil.LOGE("requestSendingMessage :: onResponse :: response = " + response);
                receiveMessage(response);
            }

            @Override
            public void onError(int status) {
                LogUtil.LOGE("requestSendingMessage :: onError");
            }
        }, message);
    }

    public void receiveMessage(String Message) {
        ReceiveChatMessage b = new Gson().fromJson(Message, ReceiveChatMessage.class);
        mChetNo = b.getChatNo();
        mChatRoomId = b.getChatRoomId();
        mMember = b.getMemberList();
        mMessage = b.getMessageText();
        mRoomExist = b.getRoomExist();
        mSender = b.getSender();
        mSenderName = b.getSenderName();
        mTime = b.getSendTime();
        LogUtil.LOGD("받은 메세지 변형 확인 ::" + mChetNo + " // " + mChatRoomId + " // " + mMember + " // " + mMessage + " // " + mRoomExist + " // " + mSender + " // " + mSenderName + " // " + mTime);
        addtext(mMessage, mSender);
        removemessage();
        scrollToEnd();
        storeDB(b);
    }

    public void storeDB(ReceiveChatMessage receiveChatMessage) {
        LogUtil.LOGV("디비 입력 시작");
        mDataManager.insertChatMessageList(receiveChatMessage);
        if (mRoomExist.equals(ROOM_TYPE_INIT)) {
            mDataManager.insertChatRoomList(receiveChatMessage);
        }
    }
}
