package com.example.administrator.homework.custom;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.util.LogUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015-01-25.
 */
public class InviteFriendListAdapter extends BaseAdapter implements CommonValues {

    private Activity mActivity;
    private LayoutInflater mInflater;
    private JsonArray mFriendListJArry;
    private ArrayList<String> mInviteMemberList;

    public InviteFriendListAdapter(Activity activity, JsonArray array) {
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
        mFriendListJArry = array;
        mInviteMemberList = new ArrayList<String>();
    }

    @Override
    public int getCount() {
        return mFriendListJArry.size();
    }

    @Override
    public JsonObject getItem(int i) {
        return mFriendListJArry.get(i).getAsJsonObject();
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.invite_friend_list_row, null);
            holder = new ViewHolder();
            holder.mProfileImage = (ProfileImageView) view.findViewById(R.id.profile);
            holder.mName = (TextView) view.findViewById(R.id.name);
            holder.mInviteCheckBox = (CheckBox) view.findViewById(R.id.invite_check);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.mInviteCheckBox.setChecked(holder.mIsChecked);
        LogUtil.LOGE("InviteFriendListAdapter :: getView() :: check = " + holder.mInviteCheckBox.isChecked());
        setInviteMemberList(getItem(i).get(PARAM_AUTH_ID).getAsString(), holder.mInviteCheckBox.isChecked());
        holder.mName.setText(getItem(i).get(PARAM_NAME).getAsString());
        holder.mProfileImage.loadImage(getItem(i).get(PARAM_PROFILE_URL).getAsString());
        return view;
    }

    private void setInviteMemberList(String memberId, boolean checked) {
        if (checked) {
            if (!mInviteMemberList.contains(memberId)) {
                mInviteMemberList.add(memberId);
            }
        } else {
            mInviteMemberList.remove(memberId);
        }
        LogUtil.LOGD("InviteFriendListAdapter :: setInviteMemberList() :: size = " + mInviteMemberList.size());
    }

    public ArrayList<String> getInviteMemberList() {
        return mInviteMemberList;
    }

    public class ViewHolder {
        public ProfileImageView mProfileImage;
        public TextView mName;
        public CheckBox mInviteCheckBox;
        public boolean mIsChecked;
    }
}
