package com.example.administrator.homework.main;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.EditText;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.LoadingView;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.util.LogUtil;

/**
 * Created by 하성재 on 2015-01-16.
 */
public class SettingDialTextActivity extends Activity implements CommonValues {
    private EditText mDialEditText;
    private DataManager mDataManager;
    private ServerProxy mServerProxy;
    private TextView mDialTextLenght;
    private TextView mClearBtn;

    private LoadingView mLoadingView;
    private RotateAnimation mLoadingAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_setting_dialtext);

        mServerProxy = ServerProxy.getInstance(this);
        mDataManager = new DataManager(SettingDialTextActivity.this);
        mDialEditText = (EditText) findViewById(R.id.dial_edittext);
        mDialEditText.setText(mDataManager.selectDialTextUserInfo());
        mDialTextLenght = (TextView) findViewById(R.id.dial_text_length);
        mClearBtn = (TextView) findViewById(R.id.deleteBtn);
        mClearBtn.setOnClickListener(mClearBtnListener);
        mDialEditText.addTextChangedListener(mLengthWatcher);

        mLoadingView = (LoadingView) findViewById(R.id.loading_area);
        mLoadingAnimation = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        mLoadingAnimation.setInterpolator(new LinearInterpolator());
        mLoadingAnimation.setRepeatCount(Animation.INFINITE);
        mLoadingAnimation.setDuration(1000);
    }

    private void showLoadingAnimation() {
        mLoadingView.setVisibility(View.VISIBLE);
        mLoadingView.startLoadingAnimation(mLoadingAnimation);
    }

    //대화명 서버로 보낸 대화명을 다시 받아 내부디비에 저장하고 mDialText에 대화명 설정하기
    private void submitDialText(String authId, String dialText) {
        mServerProxy.requestDialText(new ServerProxy.ResponseCallback() {
            @Override
            public void onResponse(String response) {
                LogUtil.LOGE("requestDialText :: onResponse :: response = " + response);
                mDataManager.updateUserDialText(response);
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onError(int status) {
                LogUtil.LOGE("requestImgUploadUrl :: onError");
            }
        }, authId, dialText);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.modify_dial_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_accept:
                showLoadingAnimation();
                submitDialText(mDataManager.selectAuthIdUserInfo(), mDialEditText.getText().toString());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private final View.OnClickListener mClearBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mDialEditText.setText("");
        }
    };

    private final TextWatcher mLengthWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mDialTextLenght.setText(Integer.toString(mDialEditText.getText().length()) + "/60");
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
}