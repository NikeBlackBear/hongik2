package com.example.administrator.homework.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

import com.example.administrator.homework.common.CommonValues;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * 앱에서 쓰이는 다양한 Util 메소드를 정의한 클래스
 */
public class Utils implements CommonValues {

    //현재 시간을 가져오는 메소드
    public static String getCurrentTime() {
        String format = new String("yy-MM-dd-HH-mm");
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.KOREA);
        return sdf.format(new Date(System.currentTimeMillis()));
    }

    //폰 번호를 가져오는 메소드
    public static String getPhoneNumber(Context context) {
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        android.util.Log.e("smjin", "phoneNumber = " + manager.getLine1Number());
        String result = manager.getLine1Number();
//        return manager.getLine1Number();
        return result.replace("+82", "0");
    }

    //연락처 변경 등의 여부로 Preference 에 새로고침 여부 flag를 변경하는 메소드
    public static void setExistsChangedData(Context context, boolean exists) {
        LogUtil.LOGE("Utils :: setExistsChangedData() exists = " + exists);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(PREF_KEY_DATA_CHANGED, exists);
        editor.apply();
    }

    //Preference 로 부터 새로고침 여부에 대한 값을 읽어오는 메소드
    public static boolean isExistsChangedData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        LogUtil.LOGE("Utils :: isExistsChangedData() exists = " + preferences.getBoolean(PREF_KEY_DATA_CHANGED, false));
        return preferences.getBoolean(PREF_KEY_DATA_CHANGED, false);
    }

    public static String getDecodeKorText(String text) {
        try {
            return URLDecoder.decode(text, "EUC-KR");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String[] timeEncoder(String time) {
        StringTokenizer timeTokenizer = new StringTokenizer(time, "-");
        String yymmdd;
        String hhmm;
        yymmdd = "20" + timeTokenizer.nextToken() + "년" + timeTokenizer.nextToken() + "월" + timeTokenizer.nextToken() + "일";
        hhmm = timeTokenizer.nextToken() + "시" + timeTokenizer.nextToken() + "분";
        String[] timeArray = {yymmdd, hhmm};
        return timeArray;
    }
}
