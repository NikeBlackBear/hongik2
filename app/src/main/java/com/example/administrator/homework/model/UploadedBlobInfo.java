package com.example.administrator.homework.model;

public class UploadedBlobInfo {
    private String authId;
    private String blobKey;
    private String profileUrl;

    public void setAuthId(String id) {
        authId = id;
    }

    public String getAuthId() {
        return authId;
    }

    public void setBlobKey(String key) {
        blobKey = key;
    }

    public String getBlobKey() {
        return blobKey;
    }

    public void setProfileUrl(String url) {
        profileUrl = url;
    }

    public String getProfileUrl() {
        return profileUrl;
    }
}
