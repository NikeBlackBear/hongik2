package com.example.administrator.homework.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.InviteFriendListAdapter;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.util.LogUtil;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Administrator on 2015-01-25.
 */
public class InviteFriendActivity extends Activity implements CommonValues {

    private ListView mInviteFriendList;
    private InviteFriendListAdapter mAdapter;
    private DataManager mDataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friend);
        init();
    }

    private void init() {
        mDataManager = new DataManager(this);
        mInviteFriendList = (ListView) findViewById(R.id.invite_friend_list);
        mInviteFriendList.setOnItemClickListener(mInviteFriendListListener);
        mAdapter = new InviteFriendListAdapter(this, (JsonArray) new JsonParser().parse(mDataManager.getFriendListToDB()));
        mInviteFriendList.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.modify_dial_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_accept:
                Intent intent = new Intent(InviteFriendActivity.this, communication.class);
                ArrayList<String> memberList = mAdapter.getInviteMemberList();
                memberList.add(mDataManager.selectAuthIdUserInfo());
                Collections.sort(memberList);
                LogUtil.LOGD("InviteFriendActivity :: onOptionsItemSelected :: sort list = " + memberList);
                intent.putStringArrayListExtra(EXTRA_RECEIVER_ID, memberList);
                startActivityForResult(intent, REQUEST_CHAT_CODE);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private final AdapterView.OnItemClickListener mInviteFriendListListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            LogUtil.LOGD("InviteFriendActivity :: OnItemClickListener :: position = " + i);
            InviteFriendListAdapter.ViewHolder holder = (InviteFriendListAdapter.ViewHolder) view.getTag();
            holder.mIsChecked = holder.mIsChecked == false ? true : false;
            view.setTag(holder);
            mAdapter.notifyDataSetChanged();
        }
    };
}