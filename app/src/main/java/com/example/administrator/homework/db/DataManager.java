package com.example.administrator.homework.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.model.ChatRoomList;
import com.example.administrator.homework.model.Friend;
import com.example.administrator.homework.model.PopUp;
import com.example.administrator.homework.model.ReceiveChatMessage;
import com.example.administrator.homework.model.UploadedBlobInfo;
import com.example.administrator.homework.util.LogUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

/**
 * 로컬 DB 와의 연동을 위한 인터페이스 역할을 하는 Class
 */
public class DataManager implements DBValues, CommonValues {
    private SQLiteDatabase mUserDatabase;
    private SQLiteDatabase mFriendDatabase;
    private SQLiteDatabase mChatRoomDatabase;
    private SQLiteDatabase mChatRoomListDatabase;

    public DataManager(Context context) {
        mUserDatabase = new UserDBHelper(context).getWritableDatabase();
        mFriendDatabase = new FriendDBHelper(context).getWritableDatabase();
        mChatRoomDatabase = new ChatRoomDBHelper(context).getWritableDatabase();
        mChatRoomListDatabase = new ChatRoomListDBHelper(context).getWritableDatabase();
    }

    public void insertNewFriendData(Friend friend) {
        LogUtil.LOGE("DataManager :: insertNewFriendData :: friend = " + friend.toString());
        ContentValues values = new ContentValues();
        values.put(COLUMN_AUTH_ID, friend.getAuthId());
        values.put(COLUMN_NAME, friend.getName());
        values.put(COLUMN_PHONE_NUMBER, friend.getPhoneNumber());
        values.put(COLUMN_DIAL_TEXT, friend.getDialText());
        values.put(COLUMN_BLOB_KEY, friend.getBlobKey());
        values.put(COLUMN_PROFILE_URL, friend.getProfileUrl());
        mFriendDatabase.insert(TABLE_FRIEND_INFO, null, values);
    }

    //로컬 DB 에 친구데이터를 삽입하는 메소드.
    public void insertOrUpdateFriendList(String response) {
        JsonArray fl = (JsonArray) new JsonParser().parse(response);
        LogUtil.LOGD("DataManager :: insertOrUpdateFriendList");
        Cursor cursor = mFriendDatabase.query(TABLE_FRIEND_INFO, null, null, null, null, null, null);
        if (cursor != null && cursor.getCount() > 0) {
            mFriendDatabase.delete(TABLE_FRIEND_INFO, null, null);
            cursor.close();
        }
        for (int i = 0; i < fl.size(); i++) {
            JsonObject friendObj = fl.get(i).getAsJsonObject();
            ContentValues values = new ContentValues();
            values.put(COLUMN_AUTH_ID, friendObj.get(PARAM_AUTH_ID).getAsString());
            values.put(COLUMN_NAME, friendObj.get(PARAM_NAME).getAsString());
            values.put(COLUMN_PHONE_NUMBER, friendObj.get(PARAM_PHONE_NUMBER).getAsString());
            values.put(COLUMN_DIAL_TEXT, friendObj.get(PARAM_DIAL_TEXT).getAsString());
            values.put(COLUMN_BLOB_KEY, friendObj.get(PARAM_BLOB_KEY).getAsString());
            values.put(COLUMN_PROFILE_URL, friendObj.get(PARAM_PROFILE_URL).getAsString());
            mFriendDatabase.insert(TABLE_FRIEND_INFO, null, values);
        }

        Cursor cCursor = mFriendDatabase.query(TABLE_FRIEND_INFO, null, null, null, null, null, null);
        if (cCursor != null) {
            LogUtil.LOGD("DataManager :: complete insertOrUpdateFriendList :: count = " + cCursor.getCount());
        } else {
            LogUtil.LOGD("DataManager :: incomplete insertOrUpdateFriendList");
        }
    }

    //로컬 DB로 부터 URL을 가져 오는 메소드
    public String getUserURLToDB() {
        String url = "";
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: getUserURLToDB :: count = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    url = cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_URL));
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
            LogUtil.LOGD("DataManager :: getUserURLToDB :: not exist db");
        }
        LogUtil.LOGD("DataManager :: getUserURLToDB :: url = " + url);
        return url;
    }

    //로컬 DB로 부터 채팅방 ID를 가져 오는 메소드
    public String getChatRoomIdToDB(String memberlist) {
        String chatRoomId = "";
        String where = COLUMN_MEMBER_LIST + " = '" + memberlist + "'";
        Cursor cursor = mChatRoomListDatabase.query(TABLE_CHAT_ROOM_LIST, null, where, null, null, null, null);
        LogUtil.LOGD("DataManager :: getChatRoomIdToDB :: memberlist = " + memberlist);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: getChatRoomIdToDB :: count = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    chatRoomId = cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ROOM_ID));
                } while (cursor.moveToNext());
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
            LogUtil.LOGD("DataManager :: getChatRoomIdToDB :: not exist db");
            cursor.close();
            return "";
        }
        LogUtil.LOGD("DataManager :: getChatRoomIdToDB :: chatRoomId = " + chatRoomId);
        cursor.close();
        return chatRoomId;
    }

    //로컬 DB 로 부터 친구 데이터를 가져오는 메소드
    public String getFriendListToDB() {
        List<Friend> list = new ArrayList<Friend>();
        Cursor cursor = mFriendDatabase.query(TABLE_FRIEND_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: getFriendListToDB :: count = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    Friend friend = new Friend();
                    friend.setAuthId(cursor.getString(cursor.getColumnIndex(COLUMN_AUTH_ID)));
                    friend.setName(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                    friend.setPhoneNumber(cursor.getString(cursor.getColumnIndex(COLUMN_PHONE_NUMBER)));
                    friend.setDialText(cursor.getString(cursor.getColumnIndex(COLUMN_DIAL_TEXT)));
                    friend.setBlobKey(cursor.getString(cursor.getColumnIndex(COLUMN_BLOB_KEY)));
                    friend.setProfileUrl(cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_URL)));
                    list.add(friend);
                } while (cursor.moveToNext());
                cursor.close();
                return new Gson().toJson(list);
            } else {
                cursor.close();
                return NOT_EXISTS_DATA;
            }
        } else {
            LogUtil.LOGD("DataManager :: getFriendListToDB :: not exist db");
            return NOT_EXISTS_DATA;
        }
    }

    public List<ChatRoomList> getChatRoomListToDB() {
        List<ChatRoomList> list = new ArrayList<ChatRoomList>();
        Cursor cursor = mChatRoomListDatabase.query(TABLE_CHAT_ROOM_LIST, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: getChatRoomListToDB :: count = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    ChatRoomList chatRoomList = new ChatRoomList();
                    chatRoomList.setChatRoomId(cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ROOM_ID)));
                    String where = COLUMN_CHAT_ROOM_ID + " = " + cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ROOM_ID));
                    Cursor cursor1 = mChatRoomDatabase.query(TABLE_CHAT_MESSAGE, null, where, null, null, null, null);
                    cursor1.moveToLast();
                    chatRoomList.setSendTime(cursor1.getString(cursor1.getColumnIndex(COLUMN_SEND_TIME)));
                    chatRoomList.setMessageText(cursor1.getString(cursor1.getColumnIndex(COLUMN_MESSAGE_TEXT)));
                    //마지막 메시지 보낸 사람의 url
                    chatRoomList.setSenderURL(getFriendProfileURL(cursor1.getString(cursor1.getColumnIndex(COLUMN_SENDER))));
                    ArrayList<String> memberList = new ArrayList<String>();
                    String memberListName = "";
                    String[] memberArray = cursor.getString(cursor.getColumnIndex(COLUMN_MEMBER_LIST)).split("/");
                    ArrayList<String> memberArrayList = new ArrayList<String>();
                    for (int i = 0; i < memberArray.length; i++) {
                        Cursor cursor3 = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
                        cursor3.moveToFirst();
                        if (!memberArray[i].equals(cursor3.getString(cursor3.getColumnIndex(COLUMN_AUTH_ID)))) {
                            LogUtil.LOGD("DataManager memberArray[" + i + "] " + memberArray[i]);
                            memberArrayList.add(memberArray[i]);
                        }
                        cursor3.close();
                    }
                    for (int i = 0; i < memberArrayList.size(); i++) {
                        LogUtil.LOGD("DataManager memberArrayList.size " + memberArrayList.size());
                        String where1 = COLUMN_AUTH_ID + " = " + memberArrayList.get(i);
                        Cursor cursor2 = mFriendDatabase.query(TABLE_FRIEND_INFO, null, where1, null, null, null, null);
                        cursor2.moveToFirst();
                        if (i == 0) {
                            memberListName = cursor2.getString(cursor2.getColumnIndex(COLUMN_NAME));
                        } else {
                            memberListName = memberListName + "," + cursor2.getString(cursor2.getColumnIndex(COLUMN_NAME));
                        }
                        cursor2.close();
                    }
                    for (int i = 0; i < memberArray.length; i++) {
                        memberList.add(memberArray[i]);
                    }
                    chatRoomList.setMemberListName(memberListName);
                    chatRoomList.setMemberList(memberList);
                    list.add(chatRoomList);
                    cursor1.close();
                }
                while (cursor.moveToNext());
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
            LogUtil.LOGD("DataManager :: getFriendListToDB :: not exist db");
        }
        return list;
    }

    //로컬 DB 에 저장 된 친구 데이터가 있는지 체크하는 메소드
    public boolean isExistFriendInfo() {
        Cursor cursor = mFriendDatabase.query(TABLE_FRIEND_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                LogUtil.LOGE("DataManager :: isExistFriendInfo() :: count = " + cursor.getCount());
                cursor.close();
                return true;
            } else {
                LogUtil.LOGE("DataManager :: isExistFriendInfo() :: count = 0");
                cursor.close();
                return false;
            }
        } else {
            LogUtil.LOGE("DataManager :: isExistFriendInfo() :: not exists db");
            return false;
        }
    }

    //로컬 DB 에 사용자 정보가 저장 되어있는지 체크하는 메소드
    public boolean isExistUserInfo() {
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                LogUtil.LOGE("DataManager :: isExistUserInfo() :: auth_id = " + cursor.getString(cursor.getColumnIndex(COLUMN_AUTH_ID)));
                cursor.close();
                return true;
            } else {
                cursor.close();
                return false;
            }
        } else {
            return false;
        }
    }

    //로컬 DB 에 사용자 정보를 삽입하는 메소드
    public void insertUserInfo(String authId, String userName) {
        LogUtil.LOGV("DataManager :: insertUserInfo() :: authId = " + authId + " userName = " + userName);
        ContentValues values = new ContentValues();
        values.put(COLUMN_AUTH_ID, authId);
        values.put(COLUMN_NAME, userName);
        values.put(COLUMN_BLOB_KEY, "");
        values.put(COLUMN_PROFILE_URL, "");
        mUserDatabase.insert(TABLE_USER_INFO, null, values);
    }

    //로컬 DB에 사용자 대화명을 변경하는 메소드
    public void updateUserDialText(String dial_text) {
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        String auth_id = null;

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                auth_id = (cursor.getString(cursor.getColumnIndex(COLUMN_AUTH_ID)));
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }
        String strFillter = COLUMN_AUTH_ID + " = " + auth_id;
        ContentValues values = new ContentValues();
        values.put(COLUMN_DIAL_TEXT, dial_text);
        mUserDatabase.update(TABLE_USER_INFO, values, strFillter, null);
    }

    //로컬 DB에서 사용자 대화명을 불러오는 메소드
    public String selectDialTextUserInfo() {
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        String dial_text = null;
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                dial_text = (cursor.getString(cursor.getColumnIndex(COLUMN_DIAL_TEXT)));
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }
        return dial_text;
    }

    //로컬 DB에서 사용자 AuthId를 불러오는 메소드
    public String selectAuthIdUserInfo() {
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        String authid = null;
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                authid = (cursor.getString(cursor.getColumnIndex(COLUMN_AUTH_ID)));
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }
        return authid;
    }

    //로컬 DB에서 사용자 이름를 불러오는 메소드
    public String selectNameUserInfo() {
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        String name = null;
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                name = (cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }
        return name;
    }

    public void updateBlobInfo(String blobKey, String profileUrl) {
        LogUtil.LOGE("DataManager :: updateBlobInfo() :: blobKey = " + blobKey + " profileUrl = " + profileUrl);
        String authId = selectAuthIdUserInfo();
        String strFillter = COLUMN_AUTH_ID + " = " + authId;
        ContentValues values = new ContentValues();
        values.put(COLUMN_BLOB_KEY, blobKey);
        values.put(COLUMN_PROFILE_URL, profileUrl);
        mUserDatabase.update(TABLE_USER_INFO, values, strFillter, null);
    }

    public String getFriendProfileURL(String friendId) {
        LogUtil.LOGE("DataManager :: getFriendProfileURL() :: friendId = " + friendId);
        String strFillter = COLUMN_AUTH_ID + " = " + friendId;
        Cursor cursor = mFriendDatabase.query(TABLE_FRIEND_INFO, null, strFillter, null, null, null, null);
        String url = "";
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                url = cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_URL));
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }

        LogUtil.LOGE("DataManager :: getFriendProfileURL() :: profileUrl = " + url);
        return url;
    }

    public UploadedBlobInfo getMyProfileBlobInfo() {
        LogUtil.LOGE("DataManager :: getMyProfileBlobInfo()");
        Cursor cursor = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
        UploadedBlobInfo info = null;
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                String blobKey = cursor.getString(cursor.getColumnIndex(COLUMN_BLOB_KEY));
                String profileUrl = cursor.getString(cursor.getColumnIndex(COLUMN_PROFILE_URL));
                LogUtil.LOGE("DataManager :: getMyProfileBlobInfo() :: blobKey = " + blobKey + " profileUrl = " + profileUrl);
                //if (blobKey != null && profileUrl != null) {
                if (!blobKey.equals("") && !profileUrl.equals("")) {
                    info = new UploadedBlobInfo();
                    info.setBlobKey(blobKey);
                    info.setProfileUrl(profileUrl);
                }
                cursor.close();
            } else {
                cursor.close();
            }
        } else {
        }

        return info;
    }

    //로컬 DB에 메시지를 저장하는 메소드
    public void insertChatMessageList(ReceiveChatMessage receiverCMessage) {
        LogUtil.LOGD("DataManager :: insertChatMessageList");
        ContentValues values = new ContentValues();
        values.put(COLUMN_SENDER, receiverCMessage.getSender());
        values.put(COLUMN_SENDER_NAME, receiverCMessage.getSenderName());
        values.put(COLUMN_MESSAGE_TEXT, receiverCMessage.getMessageText());
        values.put(COLUMN_CHAT_ROOM_ID, receiverCMessage.getChatRoomId());
        values.put(COLUMN_SEND_TIME, receiverCMessage.getSendTime());
        mChatRoomDatabase.insert(TABLE_CHAT_MESSAGE, null, values);
    }

    public String getAppendedMemberList(List<String> memberList) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < memberList.size(); i++) {
            if (i > 0) {
                builder.append("/");
            }
            builder.append(memberList.get(i));
        }
        return builder.toString();
    }

    //로컬 DB에 ChatRoomList를 저장하는 메소드
    public void insertChatRoomList(ReceiveChatMessage receiverCMessage) {
        LogUtil.LOGD("DataManager :: insertChatRoomList");
        ContentValues values = new ContentValues();
        values.put(COLUMN_CHAT_ROOM_ID, receiverCMessage.getChatRoomId());
        values.put(COLUMN_MEMBER_LIST, getAppendedMemberList(receiverCMessage.getMemberList()));
        mChatRoomListDatabase.insert(TABLE_CHAT_ROOM_LIST, null, values);
        LogUtil.LOGD("DataManager :: insertChatRoomList :: " + getAppendedMemberList(receiverCMessage.getMemberList()));
    }
    //메세지를 50개씩 가져오는 메소드
    public List<ReceiveChatMessage> loadMessage(String chatRoomId) {
        List<ReceiveChatMessage> messageListform = new ArrayList<ReceiveChatMessage>();
        String where = COLUMN_CHAT_ROOM_ID + " = '" + chatRoomId + "'";
        Cursor cursor = mChatRoomDatabase.query(TABLE_CHAT_MESSAGE, null, where, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: loadMessage :: cursorCount =" + cursor.getCount());
            if (cursor.getCount() > 0) {
                LogUtil.LOGD("DataManager :: loadMessage :: StratMessageLoading");
                cursor.moveToLast();
                int i = 1;
                do {
                    ReceiveChatMessage message = new ReceiveChatMessage();
                    LogUtil.LOGD("DataManager :: loadMessage :: " + i + "..Messageload");
                    i++;
                    message.setMessageText(cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_TEXT)));
                    message.setSendTime(cursor.getString(cursor.getColumnIndex(COLUMN_SEND_TIME)));
                    message.setSenderName(cursor.getString(cursor.getColumnIndex(COLUMN_SENDER_NAME)));
                    message.setSender(cursor.getString(cursor.getColumnIndex(COLUMN_SENDER)));
                    messageListform.add(message);
                    if (i == 50) {
                        break;
                    }
                } while (cursor.moveToPrevious());
            } else {
                LogUtil.LOGD("DataManager :: loadMessage :: (cursor.getcount <= 0) Wrong chatRoomId!!!!");
            }
        } else {
            LogUtil.LOGD("DataManager :: loadMessage :: (cursor ==null) Wrong chatRoomId!!!! ");
        }
        cursor.close();
        return messageListform;
    }

    //마지막 메세지를 가져오는 메소드
    public ReceiveChatMessage loadfinalMessage(String chatRoomId) {
        String where = COLUMN_CHAT_ROOM_ID + " = '" + chatRoomId + "'";
        ReceiveChatMessage message = new ReceiveChatMessage();
        Cursor cursor = mChatRoomDatabase.query(TABLE_CHAT_MESSAGE, null, where, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: loadMessage :: cursorCount =" + cursor.getCount());
            if (cursor.getCount() > 0) {
                LogUtil.LOGD("DataManager :: loadMessage :: StratMessageLoading");
                cursor.moveToLast();
                message.setMessageText(cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_TEXT)));
                message.setSendTime(cursor.getString(cursor.getColumnIndex(COLUMN_SEND_TIME)));
                message.setSenderName(cursor.getString(cursor.getColumnIndex(COLUMN_SENDER_NAME)));
                message.setSender(cursor.getString(cursor.getColumnIndex(COLUMN_SENDER)));
            } else {
                LogUtil.LOGD("DataManager :: loadMessage :: (cursor.getcount <= 0) Wrong chatRoomId!!!!");
            }
        } else {
            LogUtil.LOGD("DataManager :: loadMessage :: (cursor ==null) Wrong chatRoomId!!!! ");
        }
        cursor.close();
        return message;
    }

    //로컬디비에 있는 정보Alertdialog에 넣기
    public PopUp getLastMessageFromDB() {
        PopUp popUp = new PopUp();
        Cursor cursor = mChatRoomDatabase.query(TABLE_CHAT_MESSAGE, null, null, null, null, null, null);
        if (cursor != null) {
            LogUtil.LOGD("DataManager :: getChatRoomListToDB :: count = " + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToLast();
                LogUtil.LOGD("DataManager :: 고유아이디 = " + cursor.getString(cursor.getColumnIndex(COLUMN_SENDER)));
                String where = COLUMN_AUTH_ID + " = " + cursor.getString(cursor.getColumnIndex(COLUMN_SENDER));
                Cursor cursor1 = mFriendDatabase.query(TABLE_FRIEND_INFO, null, where, null, null, null, null);
                cursor1.moveToFirst();
                LogUtil.LOGD("DataManager :: 상대방 이름 = " + cursor1.getString(cursor1.getColumnIndex(COLUMN_NAME)));
                popUp.setSenderName(cursor1.getString(cursor1.getColumnIndex(COLUMN_NAME)));
                popUp.setMessageText(cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_TEXT)));
                popUp.setChatRoomId(cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ROOM_ID)));

                ArrayList<String> memberList = new ArrayList<String>();
                String where1 = COLUMN_CHAT_ROOM_ID + " = " + cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ROOM_ID));
                Cursor cursor2 = mChatRoomListDatabase.query(TABLE_CHAT_ROOM_LIST, null, where1, null, null, null, null);
                cursor2.moveToFirst();

                String[] memberArray = cursor2.getString(cursor2.getColumnIndex(COLUMN_MEMBER_LIST)).split("/");
                ArrayList<String> memberArrayList = new ArrayList<String>();
                for (int i = 0; i < memberArray.length; i++) {
                    Cursor cursor3 = mUserDatabase.query(TABLE_USER_INFO, null, null, null, null, null, null);
                    cursor3.moveToFirst();
                    if (!memberArray[i].equals(cursor3.getString(cursor3.getColumnIndex(COLUMN_AUTH_ID)))) {
                        LogUtil.LOGD("DataManager memberArray[" + i + "] " + memberArray[i]);
                        memberArrayList.add(memberArray[i]);
                    }
                    cursor3.close();
                }
                for (int i = 0; i < memberArray.length; i++) {
                    memberList.add(memberArray[i]);
                }
                popUp.setMemberList(memberList);
                popUp.setSenderURL(cursor1.getString(cursor1.getColumnIndex(COLUMN_PROFILE_URL)));
                cursor.close();
                cursor1.close();
            }
        } else {
            LogUtil.LOGD("DataManager :: getFriendListToDB :: not exist db");
        }
        return popUp;
    }
}


