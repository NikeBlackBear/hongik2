package com.example.administrator.homework.main;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.FriendListAdapter;
import com.example.administrator.homework.R;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.custom.UserInfoDialog;
import com.example.administrator.homework.custom.LoadingView;
import com.example.administrator.homework.db.DBValues;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.Contact;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.List;

public class FriendFragment extends AbsFragment implements DBValues, CommonValues {
    private LoadingView mLoadingView;
    private RelativeLayout mContent;
    private ListView mFriendList;
    private FriendListAdapter mListAdapter;

    private ServerProxy mServerProxy;
    private DataManager mDataManager;

    private JsonArray mTargetFLData;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return getLayoutInflater(savedInstanceState).inflate(R.layout.fragment_friend, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        View mainView = getView();
        mServerProxy = ServerProxy.getInstance(getActivity());
        mDataManager = new DataManager(getActivity());
        mLoadingView = (LoadingView) mainView.findViewById(R.id.loading_area);
        mLoadingView.startLoadingAnimation(getLoadingAnimation());
        mContent = (RelativeLayout) mainView.findViewById(R.id.content);
        mFriendList = (ListView) mainView.findViewById(R.id.friend_list);
        mFriendList.setOnItemClickListener(mFLItemClickListener);
        setupFriendList();
    }

    //친구 목록을 설정하는 메소드
    private void setupFriendList() {
        //먼저 로컬 DB에 친구목록 데이터가 있는지 체크한다
        if (mDataManager.isExistFriendInfo()) {
            //로컬 DB에 친구목록 데이터가 있다면, Preference 로 부터 친구목록 갱신에 대한 flag 를 체크한다.
            if (Utils.isExistsChangedData(getActivity())) {
                //연락처변경 등의 이유로 친구목록을 갱신할 필요가 있다면 서버에 연락처 데이터를 보낸 뒤 친구 데이터를 가져온다.
                LogUtil.LOGE("FriendFragment :: setupFriendList() isExistsFriendInfo && isExistsChangedData");
                mServerProxy.requestFriendList(mFriendListResponseCallback, getContacts());
            } else {
                //친구목록을 갱신할 필요가 없다면 로컬 DB에 저장 된 친구 목록을 가져온다.
                LogUtil.LOGE("FriendFragment :: setupFriendList() isExistsFriendInfo");
                showList(mDataManager.getFriendListToDB());
            }
        } else {
            //로컬 DB에 친구목록 데이터가 없다면, 서버에 연락처데이터를 보낸 뒤 친구 데이터를 가져온다
            LogUtil.LOGE("FriendFragment :: setupFriendList() need new friend list");
            mServerProxy.requestFriendList(mFriendListResponseCallback, getContacts());
        }
    }

    private void showList(String fl) {
        mTargetFLData = (JsonArray) new JsonParser().parse(fl);
        mListAdapter = new FriendListAdapter(getActivity(), mTargetFLData);
        mFriendList.setAdapter(mListAdapter);
        showContentWithStopAnimation();
    }

    private List<Contact> getContacts() {
        return ((ParentFragmentActivity) getActivity()).getContacts();
    }

    private void showContentWithStopAnimation() {
        mLoadingView.stopLoadingAnimation();
        mLoadingView.setVisibility(View.GONE);
        mContent.setVisibility(View.VISIBLE);
    }

    private final AdapterView.OnItemClickListener mFLItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            JsonObject obj = (JsonObject) mTargetFLData.get(i);
            UserInfoDialog dialog = new UserInfoDialog(obj, getActivity());
            dialog.show();
        }
    };

    private final ServerProxy.ResponseCallback mFriendListResponseCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            LogUtil.LOGE("mFriendListResponseCallback :: onResponse() :: response = " + response);
            if (!response.equals("")) {
                showList(response);
                mDataManager.insertOrUpdateFriendList(response);

                if (Utils.isExistsChangedData(getActivity())) {
                    Utils.setExistsChangedData(getActivity(), false);
                }
            } else {
                LogUtil.LOGE("mFriendListResponseCallback :: onResponse() :: no friend data");
            }
        }

        @Override
        public void onError(int status) {

        }
    };
}