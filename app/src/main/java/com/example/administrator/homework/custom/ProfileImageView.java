package com.example.administrator.homework.custom;

import android.content.Context;
import android.util.AttributeSet;

import com.android.volley.toolbox.NetworkImageView;
import com.example.administrator.homework.R;
import com.example.administrator.homework.proxy.ServerProxy;

/**
 * Created by Administrator on 2015-01-21.
 */
public class ProfileImageView extends NetworkImageView {

    private Context mContext;

    public ProfileImageView(Context context) {
        this(context, null, 0);
    }

    public ProfileImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProfileImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setDefaultImageResId(R.drawable.no_profile_image);
    }

    public void loadImage(String url) {
        setImageUrl(url, ServerProxy.getInstance(mContext).getImageLoader());
    }
}
