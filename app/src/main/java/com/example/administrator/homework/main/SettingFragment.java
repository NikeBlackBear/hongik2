package com.example.administrator.homework.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.administrator.homework.R;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.custom.LoadingView;
import com.example.administrator.homework.custom.ProfileImageView;
import com.example.administrator.homework.db.DataManager;
import com.example.administrator.homework.model.UploadedBlobInfo;
import com.example.administrator.homework.proxy.ServerProxy;
import com.example.administrator.homework.util.LogUtil;
import com.example.administrator.homework.util.Utils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;

/**
 * Created by Administrator on 2014-12-29.
 */

public class SettingFragment extends AbsFragment implements CommonValues {
    private final int REQUEST_CODE_GET_IMAGE = 0;
    private final int REQUEST_CODE_MODIFY_DIALTEXT = 1;

    private LoadingView mLoadingView;
    private DataManager mDataManager;
    private TextView mDialText;
    private FrameLayout mProfileImageBtn;
    private ServerProxy mServerProxy;
    private ProfileImageView mProfileImgView;
    private TextView mPhoneNumberView;

    private RotateAnimation mLoadingAnimation;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_setting, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View mRoot = getView();

        mServerProxy = ServerProxy.getInstance(getActivity());
        mDataManager = new DataManager(getActivity());
        mProfileImageBtn = (FrameLayout) mRoot.findViewById(R.id.profile_frame);
        mProfileImgView = (ProfileImageView) mRoot.findViewById(R.id.profile);
        mDialText = (TextView) mRoot.findViewById(R.id.dial_text);
        mPhoneNumberView = (TextView) mRoot.findViewById(R.id.phoneNumber);
        mLoadingView = (LoadingView) mRoot.findViewById(R.id.loading_area);
        mLoadingAnimation = getLoadingAnimation();
        mProfileImageBtn.setOnClickListener(mProfileImageBtnListener);
        mDialText.setOnClickListener(mDialTextClickListener);
//채팅방 테스트
        Button testbutton = (Button) mRoot.findViewById(R.id.testchat);
        testbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), communication.class);
                startActivity(intent);
            }
        });
//채팅방 테스트

        if (mDataManager.selectDialTextUserInfo() == null) {
            mDialText.setText("상태 메시지");
        } else if (mDataManager.selectDialTextUserInfo().equals("")) {
            mDialText.setText("상태 메시지");
        } else {
            mDialText.setText(mDataManager.selectDialTextUserInfo());
        }

        mPhoneNumberView.setText(Utils.getPhoneNumber(getActivity()));
        loadProfileImage();
    }

    private void loadProfileImage() {
        UploadedBlobInfo profileBlobInfo = mDataManager.getMyProfileBlobInfo();
        if (profileBlobInfo != null) {
            mProfileImgView.loadImage(profileBlobInfo.getProfileUrl());
        } else {
            LogUtil.LOGE("SettingFragment :: loadProfileImage() :: not exists profile image");
        }
    }

    private void showLoadingAnimation() {
        mLoadingView.setVisibility(View.VISIBLE);
        mLoadingView.startLoadingAnimation(mLoadingAnimation);
        mLoadingView.bringToFront();
    }

    private void dismissLoadingAnimation() {
        mLoadingView.setVisibility(View.GONE);
        mLoadingView.setAnimation(null);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //프로필 사진 가져오기
        if (requestCode == REQUEST_CODE_GET_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                getAbsolutePathFromUri(getActivity(), data.getData());
            }
            //대화명 보여주기
        } else if (requestCode == REQUEST_CODE_MODIFY_DIALTEXT) {
            if (resultCode == Activity.RESULT_OK) {
                if (mDataManager.selectDialTextUserInfo().equals("")) {
                    mDialText.setText("상태 메시지");
                } else {
                    mDialText.setText(mDataManager.selectDialTextUserInfo());
                }
            }
        }
    }

    // 프로필 사진 가져오기
    private void getAbsolutePathFromUri(Context context, Uri uri) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inSampleSize = 4;
//        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
//        mProfileImgView.setImageBitmap(bitmap);
        File profilephoto = new File(path);
        submit(profilephoto);
    }

    private void submit(final File file) {
        showLoadingAnimation();
        mServerProxy.requestImgUploadUrl(new ServerProxy.ResponseCallback() {
            @Override
            public void onResponse(String response) {
                LogUtil.LOGE("requestImgUploadUrl :: onResponse :: response = " + response);
                UploadedBlobInfo blobInfo = mDataManager.getMyProfileBlobInfo();
                String blobKey = "";
                if (blobInfo != null) {
                    blobKey = blobInfo.getBlobKey();
                }
                mServerProxy.requestImgUpload(mImageUploadCallback, file, response, mDataManager.selectAuthIdUserInfo(), blobKey);
            }

            @Override
            public void onError(int status) {
                LogUtil.LOGE("requestImgUploadUrl :: onError");
                dismissLoadingAnimation();
            }
        });
    }

    private final ServerProxy.ResponseCallback mImageUploadCallback = new ServerProxy.ResponseCallback() {
        @Override
        public void onResponse(String response) {
            JsonParser parser = new JsonParser();
            JsonObject jobj = (JsonObject) parser.parse(response);
            String blobKey = jobj.get(PARAM_BLOB_KEY).getAsString();
            String profileUrl = jobj.get(PARAM_PROFILE_URL).getAsString();
            LogUtil.LOGE("requestImgUpload :: onResponse :: response = " + response + " blobKey = " + blobKey + " profileUrl = " + profileUrl);
            mDataManager.updateBlobInfo(blobKey, profileUrl);
            mProfileImgView.setImageUrl(profileUrl, mServerProxy.getImageLoader());
            dismissLoadingAnimation();
        }

        @Override
        public void onError(int status) {
            dismissLoadingAnimation();
        }
    };

    private final View.OnClickListener mProfileImageBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(Intent.ACTION_PICK);
            i.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
            i.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, REQUEST_CODE_GET_IMAGE);
        }
    };

    private final View.OnClickListener mDialTextClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getActivity(), SettingDialTextActivity.class);
            startActivityForResult(intent, REQUEST_CODE_MODIFY_DIALTEXT);
        }
    };
}
