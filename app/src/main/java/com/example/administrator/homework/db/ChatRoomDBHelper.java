package com.example.administrator.homework.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2015-01-23.
 */
public class ChatRoomDBHelper extends SQLiteOpenHelper implements DBValues {
    private String mChatRoomId = null;

    //서버에서 받은 ChatRoomId를 받아서 넘긴다. 그 값이 대화방 아이디가 된다.
    public ChatRoomDBHelper(Context context) {
        super(context, "chatroom_database", null, 1);
    }

    // 생성된 DB가 없을 경우에 한번만 호출됨 - 채팅 내용
    @Override
    public void onCreate(SQLiteDatabase arg0) {
        String createSql = "create table " + TABLE_CHAT_MESSAGE + " ("
                + COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_SENDER + " text, "
                + COLUMN_SENDER_NAME + " text, "
                + COLUMN_MESSAGE_TEXT + " text, "
                + COLUMN_CHAT_ROOM_ID + " text, "
                + COLUMN_SEND_TIME + " text)";
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }
}
