package com.example.administrator.homework.proxy;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.administrator.homework.common.CommonValues;
import com.example.administrator.homework.model.Contact;
import com.example.administrator.homework.util.LogUtil;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerProxy implements CommonValues {
    public static final String TAG = ServerProxy.class.getSimpleName();
    private static ServerProxy mInstance;
    private ResponseCallback mResponseCallback;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;


    public static ServerProxy getInstance(Context context) {
        LogUtil.LOGE("ServerProxy :: getInstance()");
        if (mInstance == null) {
            LogUtil.LOGE("ServerProxy :: newInstance()");
            mInstance = new ServerProxy(context);
        }
        return mInstance;
    }

    private ServerProxy(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(this.mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache mCache = new LruCache(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return (Bitmap) mCache.get(url);
            }
        });
    }


    public interface ResponseCallback {
        void onResponse(String response);

        void onError(int status);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public RequestQueue getRequestQueue() {
        return mRequestQueue;
    }

    ;

    public void requestImgUploadUrl(final ResponseCallback callback) {
        LogUtil.LOGE("ServerProxy :: requestImgUploadUrl");
        StringRequest request = new StringRequest(Request.Method.POST, URL_REQUEST_IMAGE_UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public boolean requestImgUpload(final ResponseCallback callback, final File file, String uploadUrl, String authId, String blobKey) {
        LogUtil.LOGD("ServerProxy :: requestImgUpload :: uploadUrl = " + uploadUrl + " authId = " + authId + " blobKey = " + blobKey);
        MultipartRequest MPR = new MultipartRequest(uploadUrl, file, authId, blobKey,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        LogUtil.LOGD("onResponse :: response = " + response);
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                LogUtil.LOGD("onErrorResponse :: e = " + e);
                if (e != null && e.networkResponse != null) {
                    callback.onError(e.networkResponse.statusCode);
                }
            }
        });
        MPR.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(MPR);
        return true;
    }

    public void requestDialText(final ResponseCallback callback, final String authId, final String dialText) {
        LogUtil.LOGD("ServerProxy :: requestDialText() :: AuthId + DialText =" + authId + " + " + dialText);
        StringRequest request = new StringRequest(Request.Method.POST, "http://2-dot-quixotic-module-766.appspot.com/testdialtextupload",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_AUTH_ID, authId);  //물어보기 수민이한데 왜 파라미터를 쓰는지
                params.put(PARAM_DIAL_TEXT, dialText);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
    }

    public boolean requestUserAuth(final ResponseCallback callback, final String name, final String phoneNumber, final String gcmId) {
        LogUtil.LOGD("ServerProxy :: requestUserAuth() :: name = " + name + " phoneNumber = " + phoneNumber + " gcmId = " + gcmId);
        StringRequest request = new StringRequest(Request.Method.POST, URL_USER_AUTH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callback.onResponse(response);
                    }
                }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_NAME, name);
                params.put(PARAM_PHONE_NUMBER, phoneNumber);
                params.put(PARAM_GCM_ID, gcmId);
                return params;
            }
        };
        LogUtil.LOGE("requestUserAuth() :: name = " + name + " phoneNumber = " + phoneNumber);
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
        return true;
    }

    public boolean requestNotifyAboutNewUser(final ResponseCallback callback, final List<Contact> contacts, final String authId) {
        LogUtil.LOGE("requestNotifyAboutNewUser");
        StringRequest request = new StringRequest(Request.Method.POST, URL_NOTIFY_ABOUT_NEW_USER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_CONTACTS, getJsonFromContact(contacts));
                params.put(PARAM_AUTH_ID, authId);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
        return true;
    }


    public boolean requestFriendList(final ResponseCallback callback, final List<Contact> contacts) {
        LogUtil.LOGE("requestFriendList");
        StringRequest request = new StringRequest(Request.Method.POST, URL_CHECK_FRIENDLIST, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("contacts", getJsonFromContact(contacts));
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mRequestQueue.add(request);
        return true;
    }

    public void requestMessage(final ResponseCallback callback, final String sendingMessage) {
        //LogUtil.LOGE("requestSendingMessage");
        StringRequest request = new StringRequest(Request.Method.POST, URL_SENDING_MESSAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.onResponse(response);
            }
        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError e) {
                callback.onError(e.networkResponse.statusCode);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put(PARAM_MESSAGE_PACK, sendingMessage);
                return params;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        LogUtil.LOGE("requestSendingMessage :: seq = " + mRequestQueue.getSequenceNumber());
        mRequestQueue.add(request);
    }

    public String getJsonFromContact(List<Contact> contacts) {

        Gson gson = new Gson();
        LogUtil.LOGE("getJsonFromContact :: result = " + gson.toJson(contacts));

        return gson.toJson(contacts);
    }
}