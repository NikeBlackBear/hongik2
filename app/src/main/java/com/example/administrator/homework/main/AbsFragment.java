package com.example.administrator.homework.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

/**
 * 모든 Fragment 들의 공통적인 요소가 구현 된 부모 Fragment
 */
public class AbsFragment extends Fragment {
    private RotateAnimation mLoadingAnimation;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createAnimation();
    }

    private void createAnimation() {
        mLoadingAnimation = new RotateAnimation(0, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        mLoadingAnimation.setInterpolator(new LinearInterpolator());
        mLoadingAnimation.setRepeatCount(Animation.INFINITE);
        mLoadingAnimation.setDuration(1000);
    }

    protected RotateAnimation getLoadingAnimation() {
        return mLoadingAnimation;
    }
}
