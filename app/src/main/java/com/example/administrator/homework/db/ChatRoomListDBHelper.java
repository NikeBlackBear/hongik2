package com.example.administrator.homework.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2015-01-23.
 */
public class ChatRoomListDBHelper extends SQLiteOpenHelper implements DBValues {

    public ChatRoomListDBHelper(Context context) {
        super(context, "chatroomlist_database", null, 1);
    }

    // 생성된 DB가 없을 경우에 한번만 호출됨 - 대화방 아이디와 참여자
    @Override
    public void onCreate(SQLiteDatabase arg0) {
        String createSql = "create table " + TABLE_CHAT_ROOM_LIST + " ("
                + COLUMN_ID + " integer primary key autoincrement, "
                + COLUMN_CHAT_ROOM_ID + " text, "
                + COLUMN_MEMBER_LIST + " text)";
        arg0.execSQL(createSql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }
}

