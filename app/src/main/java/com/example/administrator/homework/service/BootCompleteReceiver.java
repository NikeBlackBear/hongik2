package com.example.administrator.homework.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * 폰 부팅 시 HService 시작하는 Receiver
 */
public class BootCompleteReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent sIntent = new Intent(context, HService.class);
            context.startService(sIntent);
        }
    }
}
